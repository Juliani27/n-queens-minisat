import os
from math import ceil
import numpy as np
import platform

total_lines = 0
saved_prev_res = []

''' 
Generate the board with numbers for each element and write it to input file.
'''
def generate_board(n, in_file):
	for i in range(0, n):
		line = ""
		for j in range(1, n + 1):
			el_num = (n * i) + j
			line = line + str(el_num) + " "
		line = line + "0\n"
		in_file.write(line)
		global total_lines
		total_lines += 1

'''
Generate the queen's constraint on board's rows. Queens can't be on the same row.
'''
def generate_row_constraint(n, in_file):
	for i in range(0, n):
		for j in range (1, n):
			el_num = (n * i) + j
			for k in range (1, n - j + 1):
				line = "-" + str(el_num) + " -" + str(el_num + k) + " 0\n"
				in_file.write(line)
				global total_lines
				total_lines += 1

'''
Generate the queen's constraint on board's columns. Queens can't be on the same column.
'''
def generate_column_constraint(n, in_file):
	for j in range(1, n + 1):
		for i in range(0, n):
			el_num = (n * i) + j
			for k in range(1, n - i):
				line = "-" + str(el_num) + " -" + str(el_num + (n * k)) + " 0\n"
				in_file.write(line)
				global total_lines
				total_lines += 1

'''
Generate the queen's constraint on board's diagonas. Queens can't be on the same diagonal.
'''
def generate_diagonal_constraint(n, in_file):
	el_total = n * n + 1

	for i in range(1, el_total):
		row = ceil(i / n)
		col = i % n
		if col == 0: 
			col = n

		# Left to right diagonal constraints
		for j in range(i, min((((n - col + row) * n) + 1), el_total), n + 1):
			if j == i:
				continue
			line = "-" + str(i) + " -" + str(j) + " 0\n"
			in_file.write(line)
			global total_lines
			total_lines += 1

		# Right to left diagonal constraints
		for j in range(i, el_total, n - 1):
			if j == i:
				continue
			elif ceil((j - (n - 1)) / n) == ceil(j / n):
				break
			line = "-" + str(i) + " -" + str(j) + " 0\n"
			in_file.write(line)
			total_lines += 1

'''
Generate the previous solution as constraint.
'''
def generate_prev_constraint(n, in_file):
	global saved_prev_res
	prev_res = np.reshape(saved_prev_res, (-1, n * n)).tolist()
	prev_res = prev_res[0]

	in_file.write("\n")
	line = ""
	for i in range(len(prev_res)):
		if prev_res[i] == 1:
			line = line + "-" + str(i + 1) + " "
	line = line + "0\n"
	in_file.write(line)

	global total_lines
	total_lines += 1

'''
Generate the previous result from MiniSAT syntax to a list.
'''
def generate_prev_result(n, in_file_name):
	with open(in_file_name) as in_file: 
	    lines = in_file.readlines()

	prev_constraint = lines[-1].split(" ")
	prev_constraint.pop(-1)
	
	# If back to first result
	if len(prev_constraint) != n:
		return

	prev_res = []
	for i in range(n * n):
		el_num = "-" + str(i + 1)
		if el_num in prev_constraint:
			prev_res.append(1)
		else:
			prev_res.append(0)

	prev_res = np.reshape(prev_res, (-1, n)).tolist()
	global saved_prev_res
	saved_prev_res = prev_res

	lines.pop(-1)
	with open(in_file_name, "w") as in_file: 
	    in_file.writelines(lines)

'''
Generate the placed queens position and write it as constraints.
'''
def generate_queen_requirement(n, in_file, reqs):
	all_el = []

	for req in reqs:
		el_num = (n * req[1][0]) + req[1][1] + 1
		all_el.append(el_num)

	for el in all_el:
		line = str(el) + " 0\n"
		in_file.write(line)

		global total_lines
		total_lines += 1


'''
Add 'p cnf NUMBER_OF_VARIABLES NUMBER_OF_CLAUSES' to first line in
minisat input file.
'''
def add_input_line(n, total_lines, in_file_name, next_or_prev = False):
	with open(in_file_name) as in_file: 
	    lines = in_file.readlines()

	first_line = "p cnf " + str(n * n) + " " + str(total_lines) + " \n"
	if not next_or_prev:
		lines.insert(0, first_line)
	else:
		lines[0] = first_line
	lines[-1] = lines[-1].replace("\n", "")
	 
	with open(in_file_name, "w") as in_file: 
	    in_file.writelines(lines)


'''
Convert minisat result to 2D array.
Also, convert the variable to 0 or 1.
False variable = 0 | True variable = 1
'''
def convert_minisat_result(n, out_file_name):
	with open(out_file_name) as out_file: 
		lines = out_file.readlines()

	# If all results have been generated
	if "UNSAT" in lines[0]:
		result = ["UNSAT"]
	else:
		result = lines[1].split(" ")
		result.pop(-1)
		for i in range(0, len(result)):
			if "-" in result[i]:
				result[i] = 0
			else:
				result[i] = 1

		result = np.reshape(result, (-1, n)).tolist()
		saved_next_res = result
	
	return result

def generate_first_result(n, in_file_name, reqs):
	global total_lines, saved_prev_res
	total_lines = 0
	saved_prev_res = []

	in_file = open(in_file_name, 'w+')
	generate_board(n, in_file)
	generate_row_constraint(n, in_file)
	generate_column_constraint(n, in_file)
	generate_diagonal_constraint(n, in_file)
	if reqs is not None:
		generate_queen_requirement(n, in_file, reqs)
	in_file.close()
	add_input_line(n, total_lines, in_file_name)

'''
Main function
'''
def run_minisat(n, new_res = False, next_res = False, prev_res = False, reqs = None):
	in_file_name = "nqueens.in"
	out_file_name = "nqueens.out"

	if new_res:
		generate_first_result(n, in_file_name, reqs)
	else:
		if next_res:
			in_file = open(in_file_name, 'a')
			generate_prev_constraint(n, in_file)
			in_file.close()
			add_input_line(n, total_lines, in_file_name, True)
		elif prev_res:
			generate_prev_result(n, in_file_name)
			add_input_line(n, total_lines - 1, in_file_name, True)
	
	operating_system = platform.system()
	if operating_system == "Windows":
		exe = "minisat.exe " + in_file_name + " " + out_file_name
		os.system(exe)
	else:
		exe = "minisat " + in_file_name + " " + out_file_name
		os.system(exe)

	result = convert_minisat_result(n, out_file_name)
	if result[0] == "UNSAT" and (next_res == True or prev_res == True):
		generate_first_result(n, in_file_name)
		if operating_system == "Windows":
			exe = "minisat.exe " + in_file_name + " " + out_file_name
		else:
			exe = "minisat " + in_file_name + " " + out_file_name
		os.system(exe)
		result = convert_minisat_result(n, out_file_name)
	
	global saved_prev_res
	saved_prev_res = result
	return result
