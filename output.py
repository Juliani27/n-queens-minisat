import tkinter
from tkinter.ttk import *
from PIL import Image, ImageTk


SCALE = 50
N_VALUE = 8
color1 = "#4c5c77"
color2 = "white"
placed_queens = []

def renderSquare(result, board, scale, n, q_img, check_queen = False):
    global queen_img, N_VALUE
    queen_img = q_img
    N_VALUE = n
    color = color2
    for row in range(N_VALUE):
        if N_VALUE % 2 == 0:
            color = swap_color(color, color1, color2)
        for column in range(N_VALUE):
            x1 = column * scale
            y1 = row * scale
            x2 = x1 + scale
            y2 = y1 + scale
            if check_queen:
                board.create_rectangle(x1, y1, x2, y2, fill=color, width=0, tags=f"tile{row}{column}")
                board.tag_bind(f"tile{row}{column}", "<Button-1>", 
                    lambda e, i=row, j=column, k = board : get_location(e, i, j, k))
            else:
                board.create_rectangle(x1, y1, x2, y2, fill=color, width=0)

            # Render text
            if  check_queen or result[row][column] == 0:
                text = f"({row+1},{column+1})"
                if check_queen :
                    board.create_text(x1+22,y1+25,text=text, font=("Arial", 7), \
                        fill=font_color(color,color1,color2), tags=f"tile{row}{column}")
                else:
                    board.create_text(x1+22,y1+25,text=text, font=("Arial", 7), \
                        fill=font_color(color,color1,color2))

            color = swap_color(color,color1,color2)

def get_location(event, row, column, board):
    global queen_img, placed_queens
    for queen in placed_queens:
        if queen[1] == (row, column):
            board.delete(queen[0])
            placed_queens.remove(queen)
            return

    if len(placed_queens) < N_VALUE:
        x = (50 // 2) + (column * 50)
        y = (50 // 2) + (row * 50)
        queen = board.create_image(x, y, image=queen_img, tags=f"tile{row}{column}")
        placed_queens.append([queen, (row, column)])

def swap_color(color, color1, color2):
    if color == color2:
        color = color1
    else:
        color = color2
    return color

def font_color(color,color1,color2):
    if color == color1:
        return color2
    else:
        return color1

def render_queen(position, board, scale):
    '''
    position image:
    x = SCALE//2 + (column * SCALE)
    y = SCALE//2 + (row * SCALE)
    '''
    global queen_img
    for row in range(len(position)):
        for column in range(len(position[row])):
            if position[column][row] == 1:
                x = (scale // 2) + (row * scale)
                y = (scale // 2) + (column * scale)
                board.create_image(x,y,image=queen_img)
