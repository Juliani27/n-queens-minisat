import backend
import output
import tkinter
from tkinter.ttk import *
from PIL import Image, ImageTk, ImageSequence
from playsound import playsound
from threading import Thread

'''
Return an image file from the image path with
the specified width and height.
'''
def load_image(path, width, height):
    img = Image.open(path)
    img = img.resize((width, height), Image.ANTIALIAS)
    img_result = ImageTk.PhotoImage(image=img)
    return img_result

'''
Play a sound from specified path.
'''
def playsfx(path):
    playsound(path)

'''
Play a sound but looped.
After minisat finished running, terminate it and show the output.
'''
def loopsfx(path, win):
    while True:
        playsfx(path)
        if finished:
            win.destroy()
            run_find_solution()

'''
Return the x and y position to place a window at the center
of the screen as closely as possible.
'''
def center_window(win, width, height):
    x_pos = int((win.winfo_screenwidth()/2) - (width/2))
    y_pos = int((win.winfo_screenheight()/2) - (height/2))
    return x_pos, y_pos

'''
Return the size of a widget after it's applied on screen.
'''
def get_widget_size(widget):
    widget.update()
    w = widget.winfo_reqwidth()
    h = widget.winfo_reqheight()
    return w, h

'''
Create a new popup window to show error messages.
Disable main window while popup window is active.
'''
def error_message(img_path, width, height):
    error_img = load_image(img_path, width, height)
    error_window = tkinter.Toplevel()
    error_window.title("Invalid Queens Amount")
    error_window.resizable(width=False, height=False)
    error_canvas = tkinter.Canvas(error_window, width=width, height=height, bd=-2)
    error_canvas.create_image(0, 0, image=error_img, anchor='nw')
    error_canvas.pack()
    error_x, error_y = center_window(error_window, width, height)
    error_window.geometry("{}x{}+{}+{}".format(width, height, error_x, error_y))
    error_window.grab_set()
    error_window.mainloop()

'''
Start showing error message and its sfx.
'''
def init_error(img_path, width, height, sound):
    error_thread =Thread(target=playsfx, args=[sound])
    error_thread.daemon = True
    error_thread.start()
    error_message(img_path, width, height)

'''
Exit the application if toplevel X button is pressed.
'''
def exit_toplevel():
    window.destroy()

'''
Create a new popup window to show loading message.
Play a looped sound while it's active.
'''
def loading_message(current_window, img_path, width, height):
    current_window.destroy()
    loading_img = load_image(img_path, width, height)
    loading_window = tkinter.Toplevel()
    load_thread = Thread(target=loopsfx, args=["audio/load.wav", loading_window])
    load_thread.daemon = True
    load_thread.start()
    loading_window.title("Please Wait A Moment")
    loading_window.resizable(width=False, height=False)
    loading_canvas = tkinter.Canvas(loading_window, width=480, height=320, bd=-2)
    loading_canvas.create_image(0, 0, image=loading_img, anchor='nw')
    loading_canvas.pack()
    loading_x, loading_y = center_window(loading_window, width, height)
    loading_window.geometry("{}x{}+{}+{}".format(width, height, loading_x, loading_y))
    loading_window.protocol("WM_DELETE_WINDOW", exit_toplevel)
    loading_canvas.mainloop()

'''
Go to mode 1 (find solution)
'''
def mode_1(modes_window, place_queen = False, reqs = None):
    playsfx("audio/button.wav")
    if place_queen:
        minisat_thread = Thread(target=run_backend, args=[True, reqs])
    else:
        minisat_thread = Thread(target=run_backend)
    minisat_thread.daemon = True
    minisat_thread.start()
    loading_message(modes_window, "img/loading.png", 480, 320)

'''
Open popup window to choose between 2 modes.
'''
def modes_window(img_path, width, height):
    window.withdraw()
    modes_img = load_image(img_path, width, height)
    modes_win = tkinter.Toplevel()
    modes_win.title("Please Choose The Desired Mode")
    modes_win.resizable(width=False, height=False)
    modes_canvas = tkinter.Canvas(modes_win, width=480, height=320, bd=-2)
    modes_canvas.create_image(0, 0, image=modes_img, anchor='nw')
    modes_canvas.pack()
    modes_w, modes_h = get_widget_size(modes_canvas)

    modes_x, modes_y = center_window(modes_win, width, height)
    modes_win.geometry("{}x{}+{}+{}".format(width, height, modes_x, modes_y))

    mode1_button = tkinter.Button(modes_win, image=mode1_img, command=lambda: mode_1(modes_win), borderwidth=1, relief='ridge')
    mode1_w, mode1_h = get_widget_size(mode1_button)
    mode1_button_window = modes_canvas.create_window(int((modes_w/2) - (mode1_w/2)), 0.4*(modes_h), 
    anchor='nw', window=mode1_button)

    mode2_button = tkinter.Button(modes_win, image=mode2_img, command=lambda: run_place_queen(modes_win), borderwidth=1, relief='ridge')
    mode2_w, mode2_h = get_widget_size(mode2_button)
    mode2_button_window = modes_canvas.create_window(int((modes_w/2) - (mode2_w/2)), 0.65*(modes_h), 
    anchor='nw', window=mode2_button)

    modes_win.protocol("WM_DELETE_WINDOW", lambda: main_menu(modes_win, True))

    modes_canvas.mainloop()


'''
Go back to the main menu and clear the text box input.
'''
def main_menu(output_window, modes):
    if modes == False:
        playsfx("audio/button.wav")
    output_window.destroy()
    output.placed_queens = []
    input_box.delete("1.0", tkinter.END)
    window.deiconify()

'''
Show the output visual of N Queens problem from minisat solution.
'''
def run_find_solution():
    SCALE = 50
    N_VALUE = len(result)
    bg_color = "#fbf3db"
    output_window = tkinter.Toplevel()
    output_window.resizable(width=False, height=False)
    output_window.configure(background=bg_color, width=800, height=800)
    output_thread = Thread(target=playsfx, args=["audio/output.wav"])
    output_thread.daemon = True
    output_thread.start()
    output_window.protocol("WM_DELETE_WINDOW", exit_toplevel)

    if result[0] == "UNSAT":
        output_window.title("The Result Is Unsatisfiable")
        
        # Create main menu button and background
        global unsat_img, main_menu_img
        unsat_canvas = tkinter.Canvas(output_window, width=480, height=320, bd=-2)
        unsat_canvas.create_image(0, 0, image=unsat_img, anchor='nw')
        unsat_canvas.pack()
        unsat_w, unsat_h = get_widget_size(unsat_canvas)

        main_button = tkinter.Button(output_window, image=main_menu_unsat_img, command=lambda: main_menu(output_window, False), borderwidth=1, relief='ridge')
        main_w, main_h = get_widget_size(main_button)
        main_button_window = unsat_canvas.create_window(int((unsat_w/2) - (main_w/2)), 0.6*(unsat_h), anchor='nw', window=main_button)
    else:
        output_window.configure(background=bg_color)
        output_window.title("N-Queens Result")

        # Make canvas for output window
        scrollwindow_size = 400
        board_canvas = tkinter.Canvas(output_window, borderwidth=1, width=scrollwindow_size, height=scrollwindow_size)
        board_canvas.configure(background=bg_color)
        board_canvas.grid(row=2, column=1, columnspan=N_VALUE, rowspan=N_VALUE)
        
        # Scroll bar
        hbar = tkinter.Scrollbar(output_window, orient="horizontal", command=board_canvas.xview)
        hbar.grid(row=N_VALUE+1, column=2, sticky="ew")
        vbar = tkinter.Scrollbar(output_window, orient="vertical", command=board_canvas.yview)
        vbar.grid(row=2, column=3, sticky="ns")
        max_region = N_VALUE * SCALE
        board_canvas.configure(xscrollcommand=hbar.set, yscrollcommand=vbar.set, scrollregion=(0,0,max_region, max_region))
        # Setting up scrollbar width and height
        horizontal_span = tkinter.Canvas(output_window, width=scrollwindow_size, height=1, highlightthickness=0)
        vertical_span = tkinter.Canvas(output_window, height=scrollwindow_size, width=1, highlightthickness=0)
        horizontal_span.configure(background=bg_color, borderwidth=0)
        horizontal_span.grid(row=1, column=2)
        vertical_span.configure(background=bg_color, borderwidth=0)
        vertical_span.grid(row=2, column=0)


        global queen_img
        output.renderSquare(result, board_canvas, SCALE, N_VALUE, queen_img)
        output.render_queen(result, board_canvas, SCALE)

        # Create buttons
        testvar = 1
        global prev_img
        prev_button = tkinter.Button(output_window, image=prev_img, command=lambda: loading_again(output_window, "prev"), 
        borderwidth=1, relief='ridge').grid(row=0, column=1)
        next_button = tkinter.Button(output_window, image=next_img, command=lambda: loading_again(output_window, "next"), 
        borderwidth=1, relief='ridge').grid(row=0, column=2)
        main_menu_button = tkinter.Button(output_window, image=main_menu_img, command=lambda: main_menu(output_window, False), 
        borderwidth=1, relief='ridge').grid(row=0, column=4)

    # Centering output window
    output_window.update()
    output_width = output_window.winfo_width()
    output_height = output_window.winfo_height()
    main_x, main_y = center_window(output_window, output_width, output_height)
    output_window.geometry("{}x{}+{}+{}".format(output_width, output_height, main_x, main_y))
    output_window.mainloop()

'''
Show the output visual of N Queens problem from minisat solution.
'''
def run_place_queen(modes_window):
    playsfx("audio/button.wav")
    modes_window.destroy()
    SCALE = 50
    N_VALUE = gl_queen_amount
    bg_color = "#fbf3db"
    output_window = tkinter.Toplevel()
    output_window.resizable(width=False, height=False)
    output_window.configure(background=bg_color)
    output_window.protocol("WM_DELETE_WINDOW", exit_toplevel)
    output_window.title("Place Your Own Queens")

    # Make canvas for output window
    scrollwindow_size = 400
    board_canvas = tkinter.Canvas(output_window, borderwidth=1, width=scrollwindow_size, height=scrollwindow_size)
    board_canvas.configure(background=bg_color)
    board_canvas.grid(row=2, column=1, padx=10)

    # Scroll Bar
    hbar = tkinter.Scrollbar(output_window, orient="horizontal", command=board_canvas.xview)
    hbar.grid(row=N_VALUE+1, column=1, sticky="ew")
    vbar = tkinter.Scrollbar(output_window, orient="vertical", command=board_canvas.yview)
    vbar.grid(row=2, column=2, sticky="ns")
    max_region = N_VALUE * SCALE
    board_canvas.configure(xscrollcommand=hbar.set, yscrollcommand=vbar.set, scrollregion=(0,0,max_region, max_region))
    # Setting up scrollbar width and height
    vertical_span = tkinter.Canvas(output_window, height=scrollwindow_size, width=1, highlightthickness=0)
    # horizontal_span.configure(background=bg_color, borderwidth=0)
    vertical_span.configure(background=bg_color)
    vertical_span.grid(row=2, column=0)

    global queen_img
    output.renderSquare(result, board_canvas, SCALE, N_VALUE, queen_img, True)

    # Create check solution button
    global check_img, place_queen_img
    label = Label(output_window, image = place_queen_img)
    label.image = place_queen_img
    label.grid(row=0, column=1)  
    check_button = tkinter.Button(output_window, image=check_img, 
        command=lambda: mode_1(output_window, True, output.placed_queens), 
        borderwidth=1, relief='ridge').grid(row=1, column=1, padx=0, pady=10)

    # Centering output window
    output_window.update() 
    output_width = output_window.winfo_width()
    output_height = output_window.winfo_height()
    main_x, main_y = center_window(output_window, output_width, output_height)
    output_window.geometry("{}x{}+{}+{}".format(output_width, output_height, main_x, main_y))
    output_window.mainloop()

def loading_again(output_window, target_func):
    playsfx("audio/button.wav")
    if target_func == "next":
        minisat_thread = Thread(target=next_solution, args=[gl_queen_amount])
    elif target_func == "prev":
        minisat_thread = Thread(target=prev_solution, args=[gl_queen_amount])
    minisat_thread.daemon = True
    minisat_thread.start()
    loading_message(output_window, "img/loading.png", 480, 320)

def next_solution(queen_amount):
    global result
    result = backend.run_minisat(queen_amount, next_res = True)
    global finished
    finished = True

def prev_solution(queen_amount):
    global result
    result = backend.run_minisat(queen_amount, prev_res = True)
    global finished
    finished = True

'''
Run the minisat backend code.
'''
def run_backend(place_queen = False, reqs = None):
    global result
    if place_queen:
        result = backend.run_minisat(gl_queen_amount, new_res = True, reqs = reqs)
    else:
        result = backend.run_minisat(gl_queen_amount, new_res = True)
    global finished
    finished = True

'''
Validate the amount of queen input by user.
Show error message if not valid.
Start loading window if valid.
'''
def check_queen_amount(event=None):
    try:
        playsfx("audio/button.wav")
        queen_amount = int(input_box.get("1.0", tkinter.END))
        input_box.delete("1.0", tkinter.END)
        # max_amount = 18
        if queen_amount >= 8:
            global gl_queen_amount
            gl_queen_amount = queen_amount
            modes_window("img/choose_mode.png", 480, 320)
        else:
            init_error("img/error.png", 300, 100, "audio/error.wav")
    except ValueError:
        input_box.delete("1.0", tkinter.END)
        init_error("img/error.png", 300, 100, "audio/error.wav")

if __name__ == "__main__":
    # Init global variable
    finished = False
    result = []
    gl_queen_amount = 8
    output.placed_queens = []

    # Init main window
    window = tkinter.Tk()
    
    window.title("N-Queens Problem")
    window.resizable(width=False, height=False)

    # Init images
    background = load_image("img/main.png", 480, 320)
    queen_img = load_image("img/queen.png", 40, 40)
    prev_img = load_image("img/prev.png", 96, 40)
    next_img = load_image("img/next.png", 96, 40)
    check_img = load_image("img/find_solution.png", 96, 40)
    mode1_img = load_image("img/find_solution.png", 144, 56)
    mode2_img = load_image("img/place_queens.png", 144, 56)
    place_queen_img = load_image("img/place_queen_label.png", 500, 100)
    enter_img = load_image("img/enter.png", 144, 56)
    unsat_img = load_image("img/unsat.png", 480, 320)
    main_menu_img = load_image("img/main_menu.png", 96, 40)
    main_menu_unsat_img = load_image("img/main_menu.png", 144, 56)

    canvas = tkinter.Canvas(window, width=480, height=320, bd=-2)
    canvas.create_image(0, 0, image=background, anchor='nw')
    canvas.pack()
    canvas_w, canvas_h = get_widget_size(canvas)

    # Init the textbox input and put it in main window's canvas
    input_box = tkinter.Text(canvas, width=5, height=1, font=("Montserrat", 24))
    input_w, input_h = get_widget_size(input_box)
    input_box_window = canvas.create_window(int((canvas_w/2) - (input_w/2)), 0.55*(canvas_h), anchor='nw', window=input_box)

    # Init the button to input queen amount and put it in main window's canvas
    go_button = tkinter.Button(image=enter_img, command=check_queen_amount, borderwidth=1, relief='ridge')
    go_w, go_h = get_widget_size(go_button)
    go_button_window = canvas.create_window(int((canvas_w/2) - (go_w/2)), 0.7*(canvas_h), 
    anchor='nw', window=go_button)
    window.bind('<Return>', check_queen_amount)

    # Set the main window position at the screen's center
    main_x, main_y = center_window(window, 480, 320)
    window.geometry("{}x{}+{}+{}".format(480, 320, main_x, main_y))
    window.mainloop()